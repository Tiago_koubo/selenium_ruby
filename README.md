================================

Project for testing on chrome, safari, ie, firefox browsers.

**Technologies used**
 
 - **Ruby** : It is a dynamic, open source language with a focus on simplicity and productivity. It has an elegant syntax of natural reading and easy writing. 
 - **Cucumber**: Cucumber is a tool used to perform automated acceptance tests that were created in a BDD format.
 - **Selenium**: is a set of multiplatform open source tools, used to test web applications by the browser in an automated way

**Requirements for execution**:

 - Ruby
 - Bundler gem (gem install bundler)

Steps:

 - Execute: `bundle install`
 - Attention to run with the correct profiles for your browser, tag
 - Test run example: `cucumber -t@build -p chrome`