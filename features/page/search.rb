class SearchPage
    def initialize
        get_element 'search'
    end

    def check_form_search
        wait_for_display(@map['depart_select'])
    end

    def select_depart(text)
        select_by_text(@map['depart_select'], text)
    end

    def select_return(text)
        select_by_text(@map['return_select'], text)
    end

    def click_search
        click(@map['btn_search'])
    end

    def check_message(text)
        message = get_text(@map['form_content'])
        replace_new_line_to_space(message).include?(text)
    end
end