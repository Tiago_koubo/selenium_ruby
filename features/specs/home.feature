# coding: utf-8
# language: en

@home
Feature: Home
  As a MarsAir Sales Director (Mark)
  I want potential customers to be able to go back to the flight search from anywhere on the site
  So that they are guided towards booking trips

  Acceptance criteria
  “Book a ticket to the red planet now!” should apperar somewhere prominent on the page.
  Clicking it takes the user to the home page.
  Clicking the MarsAir logo on the top left should also take the user to the home page.

  Scenario: Show the book a ticket message on the page
    Given I am on the home page
    Then I should see the message "Book a ticket to the red planet now!"

  Scenario Outline: Show home page when click on the option
    Given I am on the page <page>
    When I click on the <option> to acess the home page
    Then I should see home page

    Examples:
      |       option          |       page        |
      | Book a ticket message |  report a issue   | 
      | MarsAir Logo          |  report a issue   |
      | Book a ticket message |  search result    | 
      | MarsAir Logo          |  search result    |




