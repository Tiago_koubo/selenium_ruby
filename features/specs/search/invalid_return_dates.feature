# coding: utf-8
# language: en

@invalid_return
Feature: Invalid Return Dates
  As a MarsAir Sales Director (Mark)
  I want to prevent potential customers from searching for invalid trips
  So that they don’t waste time, and book valid ones

  Acceptance criteria
  “Unfortunately, this schedule is not possible. Please try again.” displayed when return date is less than 1 year from the departure.    


  Background:  
    Given I am on the search form travel

  Scenario: Show message when return date is less than 1 year from departure
    And I select the option "July" from departure
    And I select the option "December" from return
    When I make the search
    Then I should see the message "Unfortunately, this schedule is not possible. Please try again."

 


