# coding: utf-8
# language: en

@search
Feature: Search
  As a MarsAir Sales Director (Mark)
  I want potential customers to be able to search for flights to Mars
  So that they see what trips are available

  Acceptance criteria
  There should be ‘departure’ and ‘return’ fields on a search form.
  Flights leave every six months, in July and December, both ways.
  Trips for the next two years should be searchable.
  If there are seats, display “Seats available! Call 0800 MARSAIR to book!”
  If there are no seats, display “Sorry, there are no more seats available.”      


  Background:  
    Given I am on the search form travel

  Scenario Outline: Verify the fields in the search form
    Then I should see the field <field>

    Examples:
      |    field     | 
      |    departure |   
      |    return    |  

    
  Scenario Outline: Verify the flight leave in the both way
    When I select the field from <field>
    Then I should just see the months "July", "December"

    Examples:
      |    field     | 
      |    departure |   
      |    return    |  

  Scenario Outline: Verify that trips for next two years is searchable
    When I select the field from <field>
    Then I should see the next <year>

    Examples:
      |    field     | year   | 
      |    departure | 1 year |  
      |    return    | 1 year |  
      |    departure | 2 year |    
      |    return    | 2 year |   
   
  @build 
  Scenario: Show message that there are seats available 
    And I select the departure "July"
    And I select the return "December (two years from now)"
    When I make the search
    Then I should see the message "Seats available! Call 0800 MARSAIR to book!"

  Scenario: Show message that no seats available 
    And I select the departure that no have seat available
    And I select the return that no have seat available
    When I make the search
    Then I should see the message "Sorry, there are no more seats available."

 


