
Given("I am on the search form travel") do
    @search = SearchPage.new
    @search.check_form_search
end

And("I select the departure {string}") do | departure |
    @search.select_depart(departure)
end

And("I select the return {string}") do | returning |
    @search.select_return(returning)
end

When("I make the search") do
    @search.click_search
end

Then("I should see the message {string}") do | message | 
    expect(@search.check_message(message)).to be_truthy
end