require 'logger'

def click(el)
    $driver.find_element(:"#{el['type']}", el['value']).click
    $logger.info('Click on ' + el['value'] + ' using type of search ' + el['type'])
end
