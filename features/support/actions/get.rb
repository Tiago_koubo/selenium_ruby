require 'logger'

def get_text(el)
    $logger.info('Get text of ' + el['value'] + ' using type of search ' + el['type'])
    $driver.find_element(:"#{el['type']}", el['value']).text
end
