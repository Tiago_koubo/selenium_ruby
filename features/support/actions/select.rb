require 'logger'

def select_by_text(el, text)
    drop = $driver.find_element(el['type'], el['value'])
    choose = Selenium::WebDriver::Support::Select.new(drop)
    choose.select_by(:text, text)
    $logger.info('Click on ' + el['value'] + ' using type of search ' + el['type'] + " and choose text " + text)
end
