require 'logger'
require 'yaml'

def print 
    image = $driver.save_screenshot('image.png')
    return image
end

def embed_print(image)
    base64_img = Base64.encode64(File.open(image, 'r:UTF-8', &:read))
    attach(base64_img, 'image/png;base64')
end
  
def get_element(screen)
    dir = "#{Dir.pwd}/features/elements/#{screen}_screen.yml"
    element_map = YAML.load_file(dir)
    @map = element_map[screen]
end

def replace_new_line_to_space(text)
    return text.tr("\n", " ")
end