require 'logger'

def wait
    Selenium::WebDriver::Wait.new :timeout => 25
end

def wait_for_display(el)
  $logger.info("Waiting for element displayed #{el['value']} using type of search #{el['type']}")
  wait.until { $driver.find_element(:"#{el['type']}", el['value']).displayed? }
end