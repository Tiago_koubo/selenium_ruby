require 'yaml'
require 'report_builder'

Before do |scenario|
    $logger = Logger.new(STDOUT)
end

After do |scenario|
   image = print
   embed_print(image)
end

at_exit do
   $driver.quit
    ReportBuilder.configure do |config|
      @report = "test_result"+"_"+ENV['BROWSER']+"_"+$date_execution.delete("\n :")
      config.report_path = @report
      config.report_types = [:html]
      config.report_title = 'Test Results'
      config.additional_info = {'Browser' => "#{ENV['BROWSER'].upcase}", 'Date' => $date_execution}
      config.input_path = "cucumber.json"

    end  
    ReportBuilder.build_report   
end 
